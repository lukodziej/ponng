package com.mygdx.pong;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;


public class MyGdxGame extends ApplicationAdapter {
    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 547;
    private static final int PLAYER_SPEED = 250;
    float deltaX = -4;
    float deltaY = -2;
    int playerAPoints = 0;
    int playerBPoints = 0;
    SpriteBatch batch;
    Texture ballImg;
    Texture shoeImg1;
    Texture shoeImg2;
    Texture playgroundImg;
    Texture goalImg;
    BitmapFont font;
    CharSequence points = playerAPoints + " : " + playerBPoints;
    Sound whistleSound;
    Sound goalSound;
    Sound kick;
    private float deltaXComputer = 0;
    private float deltaYComputer = 0;
    private CollisionActor playerA;
    private CollisionActor playerB;
    private CollisionActor ball;
    private Sprite sprite;
    private Sprite sprite2;

    @Override
    public void create() {
        batch = new SpriteBatch();
        ballImg = new Texture("ball1.png");
        shoeImg2 = new Texture("shoe1.png");
        shoeImg1 = new Texture("shoe.png");
        playgroundImg = new Texture("boisko.jpg");
        goalImg = new Texture("goal.png");
        playerA = new CollisionActor(shoeImg2, 0, 0);
        playerB = new CollisionActor(shoeImg1, 730, 0);
        ball = new CollisionActor(ballImg, WINDOW_WIDTH / 2f - ballImg.getWidth() / 2f, WINDOW_HEIGHT / 2f - ballImg.getHeight() / 2f);
        font = new BitmapFont();
        sprite = new Sprite(playgroundImg);
        sprite2 = new Sprite(goalImg, -280, -105, 500, 500);

        whistleSound = Gdx.audio.newSound(Gdx.files.internal("assets/whistle.mp3"));
        goalSound = Gdx.audio.newSound(Gdx.files.internal("assets/goal.mp3"));
        kick = Gdx.audio.newSound(Gdx.files.internal("assets/kick.mp3"));
        font.getData().setScale(2);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        handleKeyboardA();
        handleKeyboardB();
        //handleComputer();
        batch.begin();
        sprite.draw(batch);
        ball.draw(batch, 1);
        ballMoves();
        playerB.draw(batch, 1);
        playerA.draw(batch, 1);
        font.draw(batch, points, 371, 520);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        ballImg.dispose();
        shoeImg2.dispose();
    }

    public static int getRandom(int[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    public void ballMoves() {
        ball.moveBy(deltaX, deltaY);
        if (ball.getX() < 0) {
            sprite2.draw(batch);
            if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                ball.setPosition(WINDOW_WIDTH / 2f - ballImg.getWidth() / 2f, WINDOW_HEIGHT / 2f - ballImg.getHeight() / 2f);
                whistleSound.play(1.0f);
                playerBPoints += 1;
                points = playerAPoints + " : " + playerBPoints;
            }
        }
        if (ball.getX() > WINDOW_WIDTH) {
            sprite2.draw(batch);
            if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                ball.setPosition(WINDOW_WIDTH / 2f - ballImg.getWidth() / 2f, WINDOW_HEIGHT / 2f - ballImg.getHeight() / 2f);
                deltaX = -4 * getRandom(new int[]{1, -1});
                deltaY = -2 * getRandom(new int[]{1, -1});
                whistleSound.play(1.0f);
                playerAPoints += 1;
                points = playerAPoints + " : " + playerBPoints;
            }
        }
        if (playerAPoints == 10 || playerBPoints == 10) {
            System.exit(0);
        }
        if (ball.getY() <= 1 || ball.getY() > WINDOW_HEIGHT - 50) {
            deltaY *= -1;
            ball.moveBy(deltaX, deltaY);
        }
        if (ball.checkCollision(playerA)) {
            kick.play(1.0f);
            deltaY *= getRandom(new int[]{1, -1});
            deltaX *= -1.2;
            ball.moveBy(deltaX, deltaY);
        }
        if (ball.checkCollision(playerB)) {
            kick.play(1.0f);
            deltaY *= getRandom(new int[]{1, -1});
            deltaX *= -1;
            ball.moveBy(deltaX, deltaY);
        }
    }

    private void handleKeyboardA() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        float deltaX = 0, deltaY = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            deltaY = PLAYER_SPEED * deltaTime;
        } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            deltaY = -PLAYER_SPEED * deltaTime;
        }
        if (deltaY != 0) { // 1
            movePlayer(deltaX, deltaY, playerA);
        }
    }

    private void handleKeyboardB() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        float deltaX = 0, deltaY = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            deltaY = PLAYER_SPEED * deltaTime;
        } else if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            deltaY = -PLAYER_SPEED * deltaTime;
        }
        if (deltaY != 0) { // 1
            movePlayer(deltaX, deltaY, playerB);
        }
    }

    private void handleComputer() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        if (playerB.getY() > WINDOW_HEIGHT - 150) {
            deltaYComputer *= -1;
        }
        if (playerB.getY() <= 20) {
            deltaYComputer = PLAYER_SPEED * deltaTime;
        }
        if (deltaYComputer != 0) {
            movePlayer(deltaXComputer, deltaYComputer, playerB);
        }
    }


    private void movePlayer(float deltaX, float deltaY, CollisionActor player) {
        player.moveBy(deltaX, deltaY);
        wallBallCollision(deltaX, deltaY, player);
    }

    private void wallBallCollision(float deltaX, float deltaY, CollisionActor player) {
        if (player.getY() < 0 || player.getY() > WINDOW_HEIGHT - 135) { // 3
            deltaY *= -1;
            player.moveBy(deltaX, deltaY); // 4
        }
    }
}
